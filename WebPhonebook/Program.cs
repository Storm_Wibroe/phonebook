﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;


namespace WebPhonebook
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var portStr = Environment.GetEnvironmentVariable("PORT");
            var port =
                string.IsNullOrEmpty(portStr)
                ? 5000
                : int.Parse(portStr);

            return WebHost.CreateDefaultBuilder(args)
                .UseUrls($"http://0.0.0.0:{port}")
                .UseStartup<Startup>();
        }
    }
}
