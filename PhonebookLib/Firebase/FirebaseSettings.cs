﻿namespace PhonebookLib
{
    public class FirebaseSettings
    {
        #region Constants

        public const string DefaultCredentialsPath = @"C:\Users\mniki\OneDrive\Documents\phonebook-cred.json";

        public const string DefaultProjectName = "phonebook-6f371";

        #endregion


        #region Properties

        /// <summary>
        /// Path to a Google credentials file.
        /// </summary>
        public string CredentialsPath { get; set; } = DefaultCredentialsPath;


        /// <summary>
        /// Firebase project name, containing the database.
        /// </summary>
        public string ProjectName { get; set; } = DefaultProjectName;

        #endregion
    }
}
